#!/bin/bash

#path used to check if service is installed
export CHECK_PATH=/etc/logstash/

# start/stop commands for different environments
export OPENSTACK_START="service logstash start"
export OPENSTACK_STOP="service logstash stop"
export DOCKER_START="/opt/logstash/bin/logstash agent -f /etc/logstash/conf.d/"

#configuration for logging
export LOG_HOST="172.24.102.12"
export LOG_PORT="5002"
export LOG_SENDING_PROTOCOL="tcp"


#parameter for es_host used in logstash-configuration.sh and logstash-run.sh and password for monit
usage() { echo "Usage: $0 [-d <string>] [-p <string>] [-e <string>] " 1>&2; exit 1; }

while getopts ":d:e:p:" o; do
    case "${o}" in
        d)
            ES_HOST=${OPTARG}
            ;;
        p)
            MONIT_PASSWORD=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${ES_HOST}" ] || [ -z "${MONIT_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ]; then
    usage
fi

export ES_HOST=${ES_HOST}
export ENVIRONMENT=${ENVIRONMENT}
echo "es_host = ${ES_HOST}"
echo "check_path = ${CHECK_PATH}"

# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of logstash
    chmod +x logstash-run.sh
    ./logstash-run.sh logstash
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
      # loads and executes script for logging to a (central) logging instance
      wget $REPOSITORY_LOGSTASH/logstash-logging.sh
      chmod +x logstash-logging.sh
      ./logstash-logging.sh
    fi
    # loads and executes script for automatic installation of logstash
    wget $REPOSITORY_LOGSTASH/logstash-install.sh
    chmod +x logstash-install.sh
    ./logstash-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_LOGSTASH/logstash-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of logstash
    wget $REPOSITORY_LOGSTASH/logstash-configuration.sh
    chmod +x logstash-configuration.sh
    ./logstash-configuration.sh

    # loads and executes script for startup of logstash
    wget $REPOSITORY_LOGSTASH/logstash-run.sh
    chmod +x logstash-run.sh
    ./logstash-run.sh logstash
fi

# installs monit for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
  wget $REPOSITORY_MONIT/monit-template.sh
  chmod +x monit-template.sh
  ./monit-template.sh -u monit -p ${MONIT_PASSWORD}
fi
