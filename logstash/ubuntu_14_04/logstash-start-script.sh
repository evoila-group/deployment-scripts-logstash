#!/bin/bash

export REPOSITORY_LOGSTASH="https://bitbucket.org/meshstack/deployment-scripts-logstash/raw/HEAD/logstash"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_LOGSTASH/logstash-template.sh --no-cache
chmod +x logstash-template.sh
./logstash-template.sh -d evoila -e openstack
