#!/bin/bash
apt-get update
apt-get install software-properties-common -y

yes \n | add-apt-repository ppa:webupd8team/java
apt-add-repository ppa:webupd8team/java -y
apt-get update
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
apt-get install oracle-java8-installer -y

echo "deb http://packages.elasticsearch.org/logstash/2.0/debian stable main" | sudo tee -a /etc/apt/sources.list

apt-get update
apt-get install logstash -y --force-yes
