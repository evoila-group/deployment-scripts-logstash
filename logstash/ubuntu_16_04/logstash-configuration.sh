#!/bin/bash
echo
echo "### Starting configuration of logstash... ###"
echo

export LS_CONF_DIR_TEMP=/home/ubuntu/logstash-conf
export LS_CONF_DIR=/etc/logstash/conf.d
export LS_CUSTOM=/etc/logstash/custom


mkdir -p $LS_CUSTOM
wget $REPOSITORY_LOGSTASH/logstash-conf/deployment_lookup.yml -O $LS_CUSTOM/deployment_lookup.yml


mkdir -p $LS_CONF_DIR_TEMP
wget $REPOSITORY_LOGSTASH/logstash-conf/000-input-start.conf -O $LS_CONF_DIR_TEMP/000-input-start.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/001-input.conf -O $LS_CONF_DIR_TEMP/001-input.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/199-input-end.conf -O $LS_CONF_DIR_TEMP/199-input-end.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/200-filter-start.conf -O $LS_CONF_DIR_TEMP/200-filter-start.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/201-prefilter.conf -O $LS_CONF_DIR_TEMP/201-prefilter.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/202-syslog-standard.conf -O $LS_CONF_DIR_TEMP/202-syslog-standard.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/203-check-if-json.conf -O $LS_CONF_DIR_TEMP/202-check-if-json.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/204-deployment.conf -O $LS_CONF_DIR_TEMP/203-deployment.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/205-setup.conf -O $LS_CONF_DIR_TEMP/204-setup.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/206-app.conf -O $LS_CONF_DIR_TEMP/205-app.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/207-app-logmessage.conf -O $LS_CONF_DIR_TEMP/206-app-logmessage.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/208-app-logmessage-app.conf -O $LS_CONF_DIR_TEMP/207-app-logmessage-app.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/209-app-logmessage-rtr.conf -O $LS_CONF_DIR_TEMP/208-app-logmessage-rtr.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/210-app-http.conf -O $LS_CONF_DIR_TEMP/209-app-http.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/211-app-error.conf -O $LS_CONF_DIR_TEMP/210-app-error.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/399-filter-end.conf -O $LS_CONF_DIR_TEMP/399-filter-end.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/400-output-start.conf -O $LS_CONF_DIR_TEMP/400-output-start.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/401-output.conf -O $LS_CONF_DIR_TEMP/401-output.conf
wget $REPOSITORY_LOGSTASH/logstash-conf/599-output-end.conf -O $LS_CONF_DIR_TEMP/599-output-end.conf


sleep 10

rm $LS_CONF_DIR/complete.conf
cat $LS_CONF_DIR_TEMP/*.conf > $LS_CONF_DIR/complete.conf
echo "Reading Logstash config from "LS_CONF_DIR"/complete.conf"

