#!/bin/bash
echo
echo "### Starting installation of Logstash ###"
echo

set -e

#accepts the Oracle JDK8 license automatically
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

#installs java8
apt-get update && apt-get install -y software-properties-common
yes \n | add-apt-repository ppa:webupd8team/java
apt-get update

apt-get install -y oracle-java8-installer
# automatically set up the Java 8 environment variables
apt-get install oracle-java8-set-default

apt-get update

#Download and install the Public Signing Key
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

apt-get install apt-transport-https

#saving repository definition to /etc/apt/sources.list.d/elastic-5.x.list
echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-5.x.list

#install logstash
apt-get update && apt-get install logstash -y --force-yes